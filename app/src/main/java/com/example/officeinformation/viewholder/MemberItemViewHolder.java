package com.example.officeinformation.viewholder;

import android.view.View;
import android.widget.TextView;

import com.example.officeinformation.R;
import com.example.officeinformation.model.Member;

public class MemberItemViewHolder extends ChildViewHolder {

    private TextView mMemberName;
    /**
     * Default constructor.
     *
     * @param itemView The {@link View} being hosted in this ViewHolder
     */
    public MemberItemViewHolder(View itemView) {
        super(itemView);
        mMemberName = itemView.findViewById(R.id.memberName);
    }

    public void bind(Member member) {
        mMemberName.setText(member.getName().toString());
    }
}
