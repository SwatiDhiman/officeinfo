package com.example.officeinformation.viewholder;

import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.officeinformation.R;
import com.example.officeinformation.model.Office;

public class OfficeItemViewHolder extends ParentViewHolder {


    private TextView mOfficeName,mOfficeWebsite,mAbout;
    private ImageView mOfficeLogo;

    /**
     * Default constructor.
     *
     * @param itemView The {@link View} being hosted in this ViewHolder
     */
    public OfficeItemViewHolder(View itemView) {
        super(itemView);
        mOfficeName = (TextView) itemView.findViewById(R.id.officeName);
        mOfficeWebsite = itemView.findViewById(R.id.officeUrl);
        mAbout = itemView.findViewById(R.id.about);
        mOfficeLogo  = itemView.findViewById(R.id.officeLogo);
    }

    public void bind(Office office) {
        mOfficeName.setText(office.getCompany());
        mOfficeWebsite.setText(office.getWebsite());
        mAbout.setText(office.getAbout());
        Glide.with(itemView).load(office.getLogo()).placeholder(R.drawable.ic_launcher_background).into(mOfficeLogo);
    }
}
