package com.example.officeinformation.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.officeinformation.model.Office;
import com.example.officeinformation.networking.OfficeRepository;

import java.util.List;

public class OfficeViewModel extends ViewModel {

    private MutableLiveData<List<Office>> mutableLiveData;
    private OfficeRepository officeRepository;

    public void init(){
        if (mutableLiveData != null){
            return;
        }
        officeRepository = OfficeRepository.getInstance();
        mutableLiveData = officeRepository.getOfficeData();

    }

    public LiveData<List<Office>> getOfficerepository() {
        return mutableLiveData;
    }

}
