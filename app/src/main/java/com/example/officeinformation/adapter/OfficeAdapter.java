package com.example.officeinformation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.officeinformation.R;
import com.example.officeinformation.items.ParentListItem;
import com.example.officeinformation.model.Member;
import com.example.officeinformation.model.Office;
import com.example.officeinformation.viewholder.MemberItemViewHolder;
import com.example.officeinformation.viewholder.OfficeItemViewHolder;

import java.util.List;

public class OfficeAdapter extends ExpandableRecyclerAdapter<OfficeItemViewHolder, MemberItemViewHolder>{

    private LayoutInflater mInflator;
    private OnItemClickListener onItemClickListener;
    /**
     * Primary constructor. Sets up  and {@link #mItemList}.
     * <p>
     * Changes to  should be made through add/remove methods in
     * {@link ExpandableRecyclerAdapter}
     *
     * @param parentItemList List of all {@link ParentListItem} objects to be
     *                       displayed in the RecyclerView that this
     *                       adapter is linked to
     */
    public OfficeAdapter(Context context, List<? extends ParentListItem> parentItemList,OnItemClickListener itemClickListener) {
        super(parentItemList);
        mInflator = LayoutInflater.from(context);
        this.onItemClickListener = itemClickListener;
    }

    public OfficeAdapter(Context context){
        mInflator = LayoutInflater.from(context);
    }

    @Override
    public OfficeItemViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View officeItemView = mInflator.inflate(R.layout.office_item_view, parentViewGroup, false);
        return new OfficeItemViewHolder(officeItemView);
    }

    @Override
    public MemberItemViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View moviesView = mInflator.inflate(R.layout.member_list_view, childViewGroup, false);
        return new MemberItemViewHolder(moviesView);
    }

    @Override
    public void onBindParentViewHolder(OfficeItemViewHolder parentViewHolder, int position, ParentListItem parentListItem) {
        Office office = (Office) parentListItem;
        parentViewHolder.bind(office);
    }

    @Override
    public void onBindChildViewHolder(MemberItemViewHolder childViewHolder, int position, Object childListItem) {
        final Member member = (Member) childListItem;
        childViewHolder.bind(member);
        childViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onItemClick(member);
            }
        });
    }


}
