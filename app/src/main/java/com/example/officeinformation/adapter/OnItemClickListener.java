package com.example.officeinformation.adapter;

import com.example.officeinformation.model.Member;

public interface OnItemClickListener {
    void onItemClick(Member member);
}
