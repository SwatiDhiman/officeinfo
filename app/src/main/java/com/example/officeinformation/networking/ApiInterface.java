package com.example.officeinformation.networking;

import com.example.officeinformation.model.Office;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("get/Vk-LhK44U")
    Observable<List<Office>> getOfficeList();
}
