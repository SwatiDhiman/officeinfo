package com.example.officeinformation.networking;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.example.officeinformation.model.Office;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfficeRepository {
    private static OfficeRepository officeRepository;

    public static OfficeRepository getInstance(){
        if (officeRepository == null){
            officeRepository = new OfficeRepository();
        }
        return officeRepository;
    }

    private ApiInterface officeApi;
    private MutableLiveData<List<Office>> officeData = new MutableLiveData<>();


    public OfficeRepository(){
        officeApi = ApiClient.getClient().create(ApiInterface.class);
    }

    public MutableLiveData<List<Office>> getOfficeData() {

        officeApi.getOfficeList().subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<List<Office>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<Office> offices) {
                if (offices.size()!=0){
                    officeData.setValue(offices);
                }
            }

            @Override
            public void onError(Throwable e) {
                officeData.setValue(null);
            }

            @Override
            public void onComplete() {

            }
        });
        return officeData;
    }
}
