package com.example.officeinformation.model;

import com.example.officeinformation.items.ParentListItem;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Expose;

import java.util.List;

public class Office implements ParentListItem {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("members")
    @Expose
    private List<Member> members = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    @Override
    public String toString() {
        return "Office{" +
                "id='" + id + '\'' +
                ", company='" + company + '\'' +
                ", website='" + website + '\'' +
                ", logo='" + logo + '\'' +
                ", about='" + about + '\'' +
                ", members=" + members +
                '}';
    }

    @Override
    public List<?> getChildItemList() {
        return members;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
