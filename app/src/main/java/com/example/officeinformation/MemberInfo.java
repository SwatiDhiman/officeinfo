package com.example.officeinformation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import static com.example.officeinformation.MainActivity.MEMBER_NAME;

public class MemberInfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_info);

        String name = getIntent().getStringExtra(MEMBER_NAME);
        TextView memberName = findViewById(R.id.memberName);
        memberName.setText(name);
    }
}
