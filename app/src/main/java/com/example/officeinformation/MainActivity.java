package com.example.officeinformation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.officeinformation.adapter.ExpandableRecyclerAdapter;
import com.example.officeinformation.adapter.OfficeAdapter;
import com.example.officeinformation.adapter.OnItemClickListener;
import com.example.officeinformation.model.Member;
import com.example.officeinformation.model.Office;
import com.example.officeinformation.viewmodels.OfficeViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String MEMBER_NAME = "MemberName";
    private OfficeViewModel officeViewModel;
    private RecyclerView recyclerView;
    private OfficeAdapter officeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rcvOfficeList);


        officeViewModel = ViewModelProviders.of(this).get(OfficeViewModel.class);
        officeViewModel.init();

        officeViewModel.getOfficerepository().observe(this, new Observer<List<Office>>() {
            @Override
            public void onChanged(List<Office> offices) {
                if (offices.size()!=0){
                    officeAdapter = new OfficeAdapter(MainActivity.this, offices, new OnItemClickListener() {
                        @Override
                        public void onItemClick(Member member) {
                            startActivity(new Intent(MainActivity.this,MemberInfo.class)
                                    .putExtra(MEMBER_NAME,member.getName().toString()));
                        }
                    });
                    recyclerView.setAdapter(officeAdapter);
                }
            }
        });

    }
}
